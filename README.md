## Description

Ce projet est une application Android pour consulter, postuler et gérer ses candidatures sur des offres d'emplois.

## Introduction

Ce projet est une application Android qui permet à un utilisateur soit de créer ses annonces soit de pouvoir postuler. Une gestion de candidatures est présente : un employeur peut consulter les candidatures qu'il a reçu ou un chercheur d'emploi peut consulter les informations qu'il a entré (CV, Lettre de motivation, etc).

## Détails

Vous pouvez aller voir mon [portefolio](https://oceaneongaro.github.io/post/interima/) pour la démo et une explication détaillée du projet.
